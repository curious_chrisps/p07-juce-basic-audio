//
//  SinOscillator.cpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 13/11/2017.
//

#include "SinOscillator.hpp"

SinOscillator::SinOscillator()
{
    frequency = 440.0f;
    phasePosition = 0.f;
    twoPi = 2 * M_PI;
    phaseIncrement = (twoPi * frequency) / sampleRate;
}

SinOscillator::~SinOscillator()
{
    
}

void SinOscillator::setSampleRate(double rate)
{
    sampleRate = rate;
}

void SinOscillator::setFrequancy(double value)
{
    frequency = value;
}

void SinOscillator::setAmplitude(float amp)
{
    amplitude = amp;
}
void SinOscillator::updateIncrement(double freq)
{
    phaseIncrement = (twoPi * freq) / sampleRate;
}

double SinOscillator::getFrequency()
{
    return frequency;
}

float SinOscillator::getSample()
{
    phasePosition += phaseIncrement;
    if (phasePosition > twoPi)
    {
        phasePosition -= twoPi;
    }
    
    return sin(phasePosition)  * amplitude;
}
