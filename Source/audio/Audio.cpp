/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.setMidiInputEnabled("VMPK Output", true);
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    if (message.isNoteOn())
    {
        osc.setFrequancy(message.getMidiNoteInHertz(message.getNoteNumber()));
        osc.setAmplitude(message.getFloatVelocity());
        isOn = true;
    }else if (message.isNoteOff())
    {
        isOn = false;
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    
    double sinVal;
    osc.updateIncrement(osc.getFrequency());
    
    while(numSamples--)
    {
        
        sinVal = osc.getSample();
        
        *outL = sinVal  * isOn;
        *outR = sinVal  * isOn;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    osc.setSampleRate(device->getCurrentSampleRate());
   
    isOn = false;

}

void Audio::audioDeviceStopped()
{

}
