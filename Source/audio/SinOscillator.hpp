//
//  SinOscillator.hpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 13/11/2017.
//

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"
#include <stdio.h>

class SinOscillator
{
public:
    SinOscillator();
    ~SinOscillator();
    
    float getSample();
    void setSampleRate(double rate);
    void setFrequancy(double value);
    void setAmplitude(float amp);
    void updateIncrement(double freq);
    double getFrequency();
    
private:
    float frequency;
    float phasePosition;
    float sampleRate;
    float amplitude;
    
    float twoPi;
    float phaseIncrement;
   
};
