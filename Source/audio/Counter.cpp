//
//  Counter.cpp
//  JuceBasicAudio - App
//
//  Created by Christoph Schick on 14/11/2017.
//

#include "Counter.hpp"

Counter::Counter() :    Thread ("CounterThread"),
                        counter (0)
{
    listener = nullptr;
}

Counter::~Counter()
{
   stopThread(500);
}

void Counter::start()
{
    counter = 0;
    startThread();
}

void Counter::stop(int timeOutMilliseconds)
{
    stopThread(timeOutMilliseconds);
}

void Counter::run()
{
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        if (listener != nullptr)
            listener->counterChanged (counter);
        Time::waitForMillisecondCounter (time + 100);
        counter = counter + 0.1;
    }
}
